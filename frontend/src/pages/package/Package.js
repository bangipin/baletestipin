import React, { useEffect } from 'react'
import { Col, Container, Row, Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPackage } from '../../store/package';

const Package = () => {
    const { itemsPackage }     = useSelector(state => state.packageReducer);
    const dispatch              = useDispatch();

    useEffect(() => {
        dispatch(fetchPackage());
    },[dispatch]);
    
    return (
        <>
            <section className="portfolio section-space">
                <Container>
                    <Row>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {itemsPackage && itemsPackage.map((item,index) => (
                                        <tr>
                                            <td key={index}>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.description}</td>
                                            <td>{item.amount}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Package