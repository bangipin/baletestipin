import React, { useEffect } from 'react'
import { Col, Container, Row, Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCustomer } from '../../store/customer';
const Customer = () => {
    const { itemsCustomer }     = useSelector(state => state.customerReducer);
    const dispatch              = useDispatch();

    useEffect(() => {
        dispatch(fetchCustomer());
    },[dispatch]);
    
    return (
        <>
            <section className="portfolio section-space">
                <Container>
                    <Row>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {itemsCustomer && itemsCustomer.map((item,index) => (
                                        <tr>
                                            <td key={index}>{item.id}</td>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>{item.phone}</td>
                                            <td>{item.address}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Customer