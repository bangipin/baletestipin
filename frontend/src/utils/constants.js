export const API_URL                    = "http://127.0.0.1:8000/api/"

export const GET_CUSTOMER               = 'GET_CUSTOMER'
export const GET_PACKAGE                = 'GET_PACKAGE'
export const GET_SALES                  = 'GET_SALES'
export const DELETE_SALES               = 'DELETE_SALES'
export const GET_SALESLINE              = 'GET_SALESLINE'

