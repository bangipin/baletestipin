import { GET_SALESLINE } from "../../utils/constants";

const initialState = {
    itemsSalesLine: [],
    isFetching: false,
    error: false
}

const salesLineReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_SALESLINE:
            return {
                ...state,
                itemsSalesLine: action.payload.data,
                isFetching: true,
                error: action.payload.errorMessage,
            };

        default:
            return state;
    }
};
export default salesLineReducer