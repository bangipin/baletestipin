<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesInvoiceLine extends Model
{
    use HasFactory;
    protected $guarded  = [];
    
    public function sales(){
        return $this->belongsTo('App\Models\SalesInvoice');
    }

    public function salespackage(){
        return $this->belongsTo('App\Models\SalesInvoice');
    }

    public function namepackage(){
        return $this->belongsTo('App\Models\Package');
    }

}
