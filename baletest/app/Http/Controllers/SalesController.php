<?php

namespace App\Http\Controllers;

use App\Models\SalesInvoice;
use App\Models\SalesInvoiceLine;
use Illuminate\Http\Request;

class SalesController extends BaseController
{
    public function index()
    {
        $salesinvoice           = SalesInvoice::get();
        if(count($salesinvoice) == 0){
            return $this->sendResponse($salesinvoice, 'successfully.');
        }else{
            foreach($salesinvoice as $db){
                $dataJson[] = [
                    'id'                    => $db->id,
                    'customer_id'           => $db->customer->name,
                    'inv_number'            => $db->inv_number,
                    'package_id'            => $db->package->package_id,
                    'inv_date'              => $db->inv_date,
                    'amount'                => $db->amount,
                    'total_discount_amount' => $db->total_discount_amount,
                    'total_amount'          => $db->total_amount,
                ];
            }        
            return $this->sendResponse($dataJson, 'successfully.');
        }
      
    }

    public function add(Request $request)
    {
        $customer                   = $request->customer;
        $packages                   = $request->packages;
        $inv                        = $request->inv;
        $date                       = $request->invdate;
        $amount                     = $request->amount;
        $discount                   = $request->discount;
        $total                      = $request->total;
        if($customer=='' || $packages=='' || $inv=='' || $date=='' || $amount=='' || $discount=='' || $total==''){
            return response()->json([
                'status' => 0,
                'message' => "<div role='alert' class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'><span aria-hidden='true'>x</span><span class='sr-only'>Close</span></button>
                            <strong>Gagal!</strong> Form is required</div>"
            ]);

        }
         
        $data = SalesInvoice::create([ 
            'customer_id'  		        => $customer,
            'inv_number' 			    => $inv,
            'inv_date'	    		    => $date,
            'amount'	    		    => $amount,
            'total_discount_amount'	    => $discount,
            'total_amount'	    		=> $total,
        ]);
        $data2 = SalesInvoiceLine::create([ 
            'sales_invoice_id'          => $data->id,
            'package_id' 			    => $packages,
            'amount'	    		    => $amount,
            'discount_amount'	        => $discount,
            'total_amount'	    		=> $total,
        ]);
        return response()->json([
            'status' => 1,
            'message' => "<div role='alert' class='alert alert-success'><button data-dismiss='alert' class='close' type='button'><span aria-hidden='true'>x</span><span class='sr-only'>Close</span></button>
                        <strong>Save!</strong> Successfully</div>"
        ],200);
    }
    public function update(Request $request,$id){
        $customer                   = $request->customer;
        $packages                   = $request->packages;
        $inv                        = $request->inv;
        $date                       = $request->invdate;
        $amount                     = $request->amount;
        $discount                   = $request->discount;
        $total                      = $request->total;
        if($customer=='' || $packages=='' || $inv=='' || $date=='' || $amount=='' || $discount=='' || $total==''){
            return response()->json([
                'status' => 0,
                'message' => "<div role='alert' class='alert alert-danger'><button data-dismiss='alert' class='close' type='button'><span aria-hidden='true'>x</span><span class='sr-only'>Close</span></button>
                            <strong>Gagal!</strong> Form is required</div>"
            ]);

        }
        $data = [ 
            'customer_id'  		        => $customer,
            'inv_number' 			    => $inv,
            'inv_date'	    		    => $date,
            'amount'	    		    => $amount,
            'total_discount_amount'	    => $discount,
            'total_amount'	    		=> $total,
        ];
        $data2 = [ 
            'package_id' 			    => $packages,
            'amount'	    		    => $amount,
            'discount_amount'	        => $discount,
            'total_amount'	    		=> $total,
        ];
        SalesInvoice::where('id',$id)->update($data);
        SalesInvoiceLine::where('sales_invoice_id',$id)->update($data2);
        return response()->json([
            'status' => 1,
            'message' => "<div role='alert' class='alert alert-success'><button data-dismiss='alert' class='close' type='button'><span aria-hidden='true'>x</span><span class='sr-only'>Close</span></button>
                        <strong>Save!</strong> Successfully</div>"
        ],200);
    }

    public function show($id){
        $db                         = SalesInvoice::find($id);
        $dataJson= [
            'id'                    => $db->id,
            'customer_id'           => $db->customer->id,
            'package_id'            => $db->package->package_id,
            'inv_number'            => $db->inv_number,
            'inv_date'              => $db->inv_date,
            'amount'                => $db->amount,
            'total_discount_amount' => $db->total_discount_amount,
            'total_amount'          => $db->total_amount,
        ];
        return $this->sendResponse($dataJson, 'successfully.');    
    }

    public function destroy($id){
        $get            = SalesInvoice::where('id',$id);
        $get->delete();
        return $this->sendResponse($get, 'successfully.');
    }
}
