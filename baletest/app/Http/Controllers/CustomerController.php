<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends BaseController
{

    public function index()
    {
        $customer           = Customer::all();
        return $this->sendResponse($customer, 'successfully.');
    }
}
