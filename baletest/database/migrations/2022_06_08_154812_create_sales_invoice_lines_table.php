<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesInvoiceLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoice_lines', function (Blueprint $table) {
            $table->id(); 
            $table->unsignedBigInteger('sales_invoice_id');
            $table->unsignedBigInteger('package_id'); 
            $table->integer('amount'); 
            $table->integer('discount_amount'); 
            $table->integer('total_amount');
            $table->foreign('sales_invoice_id')->references('id')->on('sales_invoices')->onDelete('cascade'); 
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_invoice_lines');
    }
}
