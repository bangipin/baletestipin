<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('/customer',[App\Http\Controllers\CustomerController::class,'index']);
Route::get('/package',[App\Http\Controllers\PackageController::class,'index']);

Route::get('/sales',[App\Http\Controllers\SalesController::class,'index']);
Route::post('/sales/add',[App\Http\Controllers\SalesController::class,'add']);
Route::get('/sales/{id}',[App\Http\Controllers\SalesController::class,'show']);
Route::patch('/sales/update/{id}',[App\Http\Controllers\SalesController::class,'update']);
Route::delete('/sales/{id}',[App\Http\Controllers\SalesController::class,'destroy']);